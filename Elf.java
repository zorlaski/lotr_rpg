
public class Elf extends Creature {

	public Elf() {
		super("Elf") ;
	} //Elf()

	public Elf(String n) {
		super(n);
	} //Elf(String)
	
	public int specialAbility() {
		int extraDmg = 0;
		if ((Creature.myRand.nextInt(99) + 1) <= 20)
			 extraDmg = 45 ;
		return extraDmg ;
	} //specialAbility

	public String toString() {
		return(super.toString()) ;
	} //toString
	
	public boolean equals(Object obj) {
		if (!(obj instanceof Elf)) {
			return false ;
		}
		else {
			Elf m = (Elf) obj; 
			return( this.damage == m.damage &&
					this.health == m.health &&
					this.name.compareTo(m.name) == 0) ;
		}
	} //equals

}
