
public class Ent extends Creature {

	public Ent() {
		super("Elf")
	}//Ent()

	public Ent(String n) {
		super(n);
		
	}//Ent(String)
	
	public int specialAbility() {
		int extraDmg = 0;
		if ((Creature.myRand.nextInt(99) + 1) <= 20)
			 extraDmg = 45 ;
		return extraDmg ;
	} //specialAbility

	public String toString() {
		return(super.toString()) ;
	} //toString
	
	public boolean equals(Object obj) {
		if (!(obj instanceof Ent)) {
			return false ;
		}
		else {
			Ent m = (Ent) obj; 
			return( this.damage == m.damage &&
					this.health == m.health &&
					this.name.compareTo(m.name) == 0) ;
		}
	} //equals


}
