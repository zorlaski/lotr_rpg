
public class Dunedain extends Man {

	public Dunedain() {
		super("Dunedain") ;
	}
		
	public Dunedain(String n) {
		super(n);
		
	}

	//Deals 5 bonus damage in addition to huDunedain special ability
	public int getDamageDone() {
		return (super.getDamageDone() + 5) ;
	}//getDamageDone
	
	public boolean equals(Object obj) {
		if (!(obj instanceof Dunedain)) {
			return false ;
		}
		else {
			Dunedain m = (Dunedain) obj; 
			return( this.damage == m.damage &&
					this.health == m.health &&
					this.name.compareTo(m.name) == 0) ;
		}
	} //equals
}
