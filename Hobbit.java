

public class Hobbit extends Creature {
	
	public Hobbit() {
		super("Hobbit") ;
	}
	
	public Hobbit(String n) {
		super(n) ;
	}
	
	public int specialAbility() {
		int extraDmg = 0;
		if ((Creature.myRand.nextInt(99) + 1) <= 30)
			 extraDmg = 15 ;
		return extraDmg ;
	}
	
	public String toString() {
		return(super.toString()) ;
	} //toString
	
	public boolean equals(Object obj) {
		if (!(obj instanceof Hobbit)) {
			return false ;
		}
		else {
			Hobbit h = (Hobbit) obj; 
			return( this.damage == h.damage &&
					this.health == h.health &&
					this.name.compareTo(h.name) == 0) ;
		}
	} //equals
}