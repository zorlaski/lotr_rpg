
public class UrukHai extends Orc {

	public UrukHai() {
		super("UrukHai") ;
	} //UrukHai()

	public UrukHai(String n) {
		super(n);
		
	} //UrukHai(String)

	public int specialAbility() {
		int extraDmg = 0;
		if ((Creature.myRand.nextInt(99) + 1) <= 30)
			 extraDmg = 20 ;
		return extraDmg ;
	} //specialAbility
	
	public String toString() {
		return(super.toString()) ;
	} //toString
	
	public boolean equals(Object obj) {
		if (!(obj instanceof UrukHai)) {
			return false ;
		}
		else {
			UrukHai m = (UrukHai) obj; 
			return( this.damage == m.damage &&
					this.health == m.health &&
					this.name.compareTo(m.name) == 0) ;
		}
}
