
public class Druedain extends Man {

	public Druedain() {
		super("Druedain") ;
	} //Druedain()
	
	public Druedain(String n) {
		super(n);
	}//Druedain(n)
	
	public int getDamageDone() {
		return(super.getDamageDone() + 3) ;
	}//getDamageDone()
	
	public boolean equals(Object obj) {
		if (!(obj instanceof Druedain)) {
			return false ;
		}
		else {
			Druedain m = (Druedain) obj; 
			return( this.damage == m.damage &&
					this.health == m.health &&
					this.name.compareTo(m.name) == 0) ;
		}//equals
	}
}