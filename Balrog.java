
public class Balrog extends Ainur {

	public Balrog() {
		super("Balrog") ;
	}//Balrog()

	public Balrog(String n) {
		super(n);
	}//Balrog(String)

	public boolean equals(Object obj) {
		if (!(obj instanceof Balrog)) {
			return false ;
		}
		else {
			Balrog m = (Balrog) obj; 
			return( this.damage == m.damage &&
					this.health == m.health &&
					this.name.compareTo(m.name) == 0) ;
		}
	} //equals
}
