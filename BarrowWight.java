
public class BarrowWight extends Creature {

	public BarrowWight() {
		super("Barrow Wight") ;
	} //BarrowWight()

	public BarrowWight(String n) {
		super(n);
	} //BarrowWight(String)
	
	public int specialAbility() {
		int extraDmg = 0;
		if ((Creature.myRand.nextInt(99) + 1) <= 30)
			 extraDmg = 20 ;
		return extraDmg ;
	} //specialAbility
	
	public String toString() {
		return(super.toString()) ;
	} //toString
	
	public boolean equals(Object obj) {
		if (!(obj instanceof BarrowWight)) {
			return false ;
		}
		else {
			BarrowWight m = (BarrowWight) obj; 
			return( this.damage == m.damage &&
					this.health == m.health &&
					this.name.compareTo(m.name) == 0) ;
		}
	} //equals
}
