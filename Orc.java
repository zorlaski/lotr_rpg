
public class Orc extends Creature {

	public Orc() {
		super("Orc") ;
	} //Orc()

	public Orc(String n) {
		super(n);
		// TODO Auto-generated constructor stub
	} //Orc(String)

	public int specialAbility() {
		int extraDmg = 0;
		if ((Creature.myRand.nextInt(99) + 1) <= 30)
			 extraDmg = 20 ;
		return extraDmg ;
	} //specialAbility
	
	public String toString() {
		return(super.toString()) ;
	} //toString
	
	public boolean equals(Object obj) {
		if (!(obj instanceof Orc)) {
			return false ;
		}
		else {
			Orc m = (Orc) obj; 
			return( this.damage == m.damage &&
					this.health == m.health &&
					this.name.compareTo(m.name) == 0) ;
		}
}
