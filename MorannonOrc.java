
public class MorannonOrc extends Orc {

	public MorannonOrc() {
		super("Morannon") ;
	}

	public MorannonOrc(String n) {
		super(n);
	}

	public boolean equals(Object obj) {
		if (!(obj instanceof MorannonOrc)) {
			return false ;
		}
		else {
			MorannonOrc m = (MorannonOrc) obj; 
			return( this.damage == m.damage &&
					this.health == m.health &&
					this.name.compareTo(m.name) == 0) ;
		}
	}
}