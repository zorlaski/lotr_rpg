
public abstract class Ainur extends Creature{

	public Ainur() {
		super() ;
	}
	
	public Ainur(String n) {
		super(n) ;
	}
	
	public int getDamageDone() {
		int damageDone = myRand.nextInt(damage) ;
		if ((Creature.myRand.nextInt(99) + 1) <= 5) ;
			damageDone *= 3 ;
		return damageDone ;
	} //getDamageDone
	
	public String toString() {
		return(super.toString()) ;
	} //toString
	
	public boolean equals(Object obj) {
		if (!(obj instanceof Ainur)) {
			return false ;
		}
		else {
			Ainur m = (Ainur) obj; 
			return( this.damage == m.damage &&
					this.health == m.health &&
					this.name.compareTo(m.name) == 0) ;
		}
	} //equals
}