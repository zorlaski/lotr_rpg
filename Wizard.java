
public class Wizard extends Ainur {

	public Wizard() {
		super("Wizard") ;
	}

	public Wizard(String n) {
		super(n);
	}
	
	public boolean equals(Object obj) {
		if (!(obj instanceof Wizard)) {
			return false ;
		}
		else {
			Wizard m = (Wizard) obj; 
			return( this.damage == m.damage &&
					this.health == m.health &&
					this.name.compareTo(m.name) == 0) ;
		}
	} //equals
}
