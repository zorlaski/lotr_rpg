public class WatcherInTheWater extends Creature{

	public WatcherInTheWater() {
		super("Watcher In The Water") ;
	}

	public int getDamageDone() {
		int damageDone = myRand.nextInt(damage) ;
		if ((Creature.myRand.nextInt(99) + 1) <= 10) ;
			damageDone *= 3 ;
		return damageDone ;
	} //getDamageDone

	public String toString() {
		return(super.toString()) ;
	} //toString
	
	public boolean equals(Object obj) {
		if (!(obj instanceof WatcherInTheWater)) {
			return false ;
		}
		else {
			WatcherInTheWater m = (WatcherInTheWater) obj; 
			return( this.damage == m.damage &&
					this.health == m.health &&
					this.name.compareTo(m.name) == 0) ;
		}
	} //equals
}
