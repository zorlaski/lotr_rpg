
public class Dwarf extends Creature {

	public Dwarf() {
		super("Dwarf") ;
	} //Dwarf()

	public Dwarf(String n) {
		super(n);
	} //Dwarf(String)
	
	public int specialAbility() {
		int extraDmg = 0;
		if ((Creature.myRand.nextInt(99) + 1) <= 30)
			 extraDmg = 20 ;
		return extraDmg ;
	} //specialAbility
	
	public String toString() {
		return(super.toString()) ;
	} //toString
	
	public boolean equals(Object obj) {
		if (!(obj instanceof Dwarf)) {
			return false ;
		}
		else {
			Dwarf m = (Dwarf) obj; 
			return( this.damage == m.damage &&
					this.health == m.health &&
					this.name.compareTo(m.name) == 0) ;
		}
	} //equals
}
