
public class Rohirrim extends Man {

	public Rohirrim() {
		super("Rohirrim") ;
	}

	public Rohirrim(String n) {
		super(n);
	}

	public int getDamageDone() {
		return((int)(super.getDamageDone() * 1.15)) ;
	}
	public boolean equals(Object obj) {
		if (!(obj instanceof Rohirrim)) {
			return false ;
		}
		else {
			Rohirrim m = (Rohirrim) obj; 
			return( this.damage == m.damage &&
					this.health == m.health &&
					this.name.compareTo(m.name) == 0) ;
		}
}
