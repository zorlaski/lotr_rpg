
public class Troll extends Creature {

	public Troll() {
		super("Troll") ;
	} //Troll

	public Troll(String n) {
		super(n) ;
	} //Troll(String) 
	
	public int specialAbility() {
		int extraDmg = 0;
		if ((Creature.myRand.nextInt(99) + 1) <= 20)
			 extraDmg = 40 ;
		return extraDmg ;
	} //specialAbility

	public String toString() {
		return(super.toString()) ;
	} //toString
	
	public boolean equals(Object obj) {
		if (!(obj instanceof Troll)) {
			return false ;
		}
		else {
			Troll m = (Troll) obj; 
			return( this.damage == m.damage &&
					this.health == m.health &&
					this.name.compareTo(m.name) == 0) ;
		}
	} //equals
}
