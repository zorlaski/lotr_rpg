
public class Sindar extends Teleri {

	public Sindar() {
		super("Sindar") ;
	}

	public Sindar(String n) {
		super(n);
	}
	
	public boolean equals(Object obj) {
		if (!(obj instanceof Sindar)) {
			return false ;
		}
		else {
			Sindar m = (Sindar) obj; 
			return( this.damage == m.damage &&
					this.health == m.health &&
					this.name.compareTo(m.name) == 0) ;
		}
	} //equals

}
