/* Zack Orlaski ~ COMP 220 ~ Child class of Man */

public class Man extends Creature {

	public Man() {
		super("Man") ;
	} //Man()


	public Man(String n) {
		super(n);
	} //Man(String)
	
	public int specialAbility() {
		int extraDmg = 0;
		if ((Creature.myRand.nextInt(99) + 1) <= 30)
			 extraDmg = 5 ;
		return extraDmg ;
	} //specialAbility
		
	public boolean equals(Object obj) {
		if (!(obj instanceof Man)) {
			return false ;
		}
		else {
			Man m = (Man) obj; 
			return( this.damage == m.damage &&
					this.health == m.health &&
					this.name.compareTo(m.name) == 0) ;
		}
	} //equals

}
