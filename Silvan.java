
public class Silvan extends Elf {

	public Silvan() {
		super("Silvan") ;
	}

	public Silvan(String n) {
		super(n);
		
	}
	
	public boolean equals(Object obj) {
		if (!(obj instanceof Silvan)) {
			return false ;
		}
		else {
			Silvan m = (Silvan) obj; 
			return( this.damage == m.damage &&
					this.health == m.health &&
					this.name.compareTo(m.name) == 0) ;
		}
	} //equals

}