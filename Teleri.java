
public class Teleri extends Elf {

	public Teleri() {
		super("Teleri") ;
	}

	public Teleri(String n) {
		super(n);
		
	}
	
	public boolean equals(Object obj) {
		if (!(obj instanceof Teleri)) {
			return false ;
		}
		else {
			Teleri m = (Teleri) obj; 
			return( this.damage == m.damage &&
					this.health == m.health &&
					this.name.compareTo(m.name) == 0) ;
		}
	} //equals

}
