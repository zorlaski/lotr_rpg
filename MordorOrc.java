public class MordorOrc extends Orc {

	public MordorOrc() {
		super("Mordor") ;
	}

	public MordorOrc(String n) {
		super(n);
	}

	public boolean equals(Object obj) {
		if (!(obj instanceof MordorOrc)) {
			return false ;
		}
		else {
			MordorOrc m = (MordorOrc) obj; 
			return( this.damage == m.damage &&
					this.health == m.health &&
					this.name.compareTo(m.name) == 0) ;
		}
	}
}