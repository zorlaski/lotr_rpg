/* Zack Orlaski ~ COMP 220 ~ Main driver for lotr_rpg */
public class MainDriver {

	public static void main(String[] args) {
		
		Hobbit hobbit1, hobbit2 ;
		hob1 = new Hobbit("Sam") ;
		hob2 = new Hobbit("Bubba") ;
		int damage1, damage2 ;
		int health1, health2;
		
		while(hobbit1.getHealth() > 0 && hobbit2.getHealth() > 0){
			damage1 = hobbit1.getDamage() ;
			damage2 = hobbit2.getDamage() ;
			
			damage1 += hobbit1.getSpecialAbility() ;
			damage2 += hobbit2.getSpecialAbility() ;
			
			health1 -= damage2 ;
			health2 -= damage1 ;
			
			hobbit1.setHealth(health1);
			hobbit2.setHealth(health2);
		}
		
		if(hobbit1.getHealth() > 0) {
			System.out.println(hobbit1.getName() + " is Victorious") ;			
		}
		else if(hobbit2.getHealth() > 0) {
			System.out.println(hobbit1.getName() + " is Victorious") ;
		}
		else
			System.out.println("Both fighters have perished.") ;
		
		System.out.println(m1 + "\n" + m2) ;
	}

}
