/*
 * Zack Orlaski ~ COMP 220 ~ Abstract parent class for all creatures of lotr_rpg
 */

import java.util.Random; 
public abstract class Creature /*implements comparable*/ {
	protected int health ;  //damage creature can inflict
	protected int damage;
	protected String name ; //name of creature
	
	/* Min and max damage and health values */
	private final int MIN_HEALTH = 100 ;
	private final int MAX_HEALTH = 200 ;
	private final int MIN_DAMAGE = 25 ;
	private final int MAX_DAMAGE = 75 ;
	public static Random myRand = new Random() ;
	
	/* Constructors */
	public Creature() {
		health = myRand.nextInt(MAX_HEALTH - MIN_HEALTH) + MIN_HEALTH ;
		damage = myRand.nextInt(MAX_DAMAGE - MIN_DAMAGE) + MIN_DAMAGE ;
		name = "creature" ;
	} //Creature()
	
	
	
	public Creature(String n) {
		health = myRand.nextInt(MAX_HEALTH - MIN_HEALTH) + MIN_HEALTH ;
		damage = myRand.nextInt(MAX_DAMAGE - MIN_DAMAGE) + MIN_DAMAGE ;
		name = n ;
	} //Creature(String)
	
	
	/* Accessors */
	public int getDamage() {
		return damage ;
	} //getDamage
	public int getHealth() {
		return health ;
	} //getHealth
	
	public String getName() {
		return name ;
	} //getName
	
	//Deals damage within randomly generated bounds of the creature
	public int getDamageDone() {
		return(myRand.nextInt(damage) + specialAbility()) ;
	}//getDamageDone
	
	/* Mutators */
	public void setHealth(int h) {
		if (h > MAX_HEALTH)
			health = MAX_HEALTH ;
		else if (h < MIN_HEALTH)
			health = MIN_HEALTH ;
		else health = h ;
	} //setHealth
	//Overriden in child classes, man has no special abilities
	public int specialAbility(){
		return 0  ;
	}
	
	public void setDamage(int d) {
		if (d > MAX_DAMAGE)
			damage = MAX_DAMAGE;
		else if (d < MIN_DAMAGE)
				damage = MIN_DAMAGE ;
		else 
				damage = d ;
	} //setDamage
	
	public void setName(String n) {
		name = n ;
	} //setName
	
	
	public String toString(){
		return("Creature Type: " + this.getClass() +  "\nCreature Name: " +  this.getName() + "\nCreature Health: " + this.getHealth() + "\nCreature Damage: " + this.getDamage()) ;
	} //toString
	
	//getRandBetween
	//getDamageDone
	//compareTo
	//hashCode
	
}
